﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class Health : MonoBehaviour 
{
[SerializeField] private int health;
public Action <int , GameObject> OnTakeHit;
public int CurrentHealth
{
  get{return health;}
}
public int health1
{
 get {return health;}
 set
 {
      if (value >200)
      health =value;
 }
}


private void Start()
{
  GameManager.Instance.healthContainer.Add(gameObject,this);
  GameManager.Instance.collisionContainer.Add(gameObject,this);
}
public void TakeHit(int damage,GameObject attacker)
{
  
	health -= damage;
  if(OnTakeHit!=null)
  OnTakeHit(damage, attacker);
	if(health<=0)
	Destroy(gameObject);
}
public void SetHealth(int bonusHealth)
{

health+= bonusHealth;

}
public void OnTriggerEnter2D(Collider2D col)
 {
   if(col.gameObject.CompareTag("Tablet"))
   {
     Destroy(col.gameObject);
     SetHealth(10);
   }
 }
}

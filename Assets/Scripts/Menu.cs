﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour 
{
	[SerializeField] private InputField nameField;
	public Button level2b;
	public Button level3b;
	int levelComplete;
	private void Start()

	{
		levelComplete =PlayerPrefs.GetInt("LevelComplete");
		level2b.interactable=false;
		level3b.interactable=false;
		switch (levelComplete)
		{
			case 1:
			{
				level2b.interactable =true;
				break;
			}
			case 2:
			{
				level2b.interactable =true;
				level3b.interactable =true;
				break;

			}

		}
		if(PlayerPrefs.HasKey("Player_Name"))
		nameField.text=PlayerPrefs.GetString("Player_Name");
	}
	public void  OnEndEditName()
	{
		PlayerPrefs.SetString("Player_Name",nameField.text);
	}

	public void LoadTo(int level)
	{
     SceneManager.LoadScene(level);
	}
	public void Reset()
	{
		level2b.interactable=false;
		level3b.interactable=false;
		PlayerPrefs.DeleteAll();
	}

 public void OnClickPlay()
 {
 SceneManager.LoadScene(1);
 }
 public void OnClickExit()
 { 
 Application.Quit();
 }
	
}

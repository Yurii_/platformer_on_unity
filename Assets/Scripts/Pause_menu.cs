﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Pause_menu : MonoBehaviour 
{
 
 public static bool GameIsPaused =false;
 bool isSoundActive;
 public GameObject PauseMenuUI;
 
 private void Update()
 {
 if(Input.GetKeyDown(KeyCode.Escape))
 {
	if(GameIsPaused)
	{
    Continue();
	}
	else
	{
		Pause();
	}
 }
 }
 public void Continue()
 {
 PauseMenuUI.SetActive(false); 
 Time.timeScale =1f;
 GameIsPaused=false;
 }

  void Pause()
 {
 PauseMenuUI.SetActive(true); 
 Time.timeScale =0f;
 GameIsPaused=true;	
 }
 public void LoadMenu()
 {
   Time.timeScale =1f;
   SceneManager.LoadScene("GameMenu");
 }
 public void Exit()
 {
 Application.Quit();
 }
 
 public void OnMouseDown() 
 {
    if (isSoundActive) 
	{
        transform.GetChild(0).GetComponent<Text>().text = "On";
	}
    else 
	{
        transform.GetChild(0).GetComponent<Text>().text = "Off";
		isSoundActive = !isSoundActive;
	}
 }
 
}
